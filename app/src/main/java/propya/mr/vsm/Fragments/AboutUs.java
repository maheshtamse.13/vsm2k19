package propya.mr.vsm.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import propya.mr.vsm.Activities.MainIntroActivity;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.Story.TimerService;
import propya.mr.vsm.UI.AppStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUs extends Fragment {

    public Button splash,timer,beacon,getText;
    public View rootView;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewSetter();
    }


    public AboutUs() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        AppStatus.currentContextPrimary = getContext();
        super.onResume();
        AppStatus.fragment = 3;
    }


    @Override
    public void onPause() {
        super.onPause();
        AppStatus.currentContextPrimary = null;
        AppStatus.fragment = -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_about_us, container, false);
        return rootView;
    }


    void viewSetter(){
        splash=(Button)rootView.findViewById(R.id.button);
        splash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MainIntroActivity.class));
            }
        });
        timer = (Button)rootView.findViewById(R.id.buttonTimer);
        timer.setText("Clear");
        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TimerService.secs = 60;
//                getContext().startService(new Intent(getActivity(),TimerService.class));
                clearPref();
            }
        });
        beacon = (Button)rootView.findViewById(R.id.beaconButton);
        beacon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity)getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment,new WaitForBeacon()).addToBackStack(null).commit();
            }
        });


        beacon.setText(Integer.toString(GetData.getRound()));

        getText = (Button)rootView.findViewById(R.id.getText);
        getText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetData.setCash(getContext(), Constants.initailWorth);

                final GetString getString = new GetString();
                final String code = new String("da");



                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        getString.dismiss();
             //           Toast.makeText(getContext(), "String "+GetString.code, Toast.LENGTH_SHORT).show();


                    }
                };

                getString.setParams(listener);
                getString.show(getChildFragmentManager(),"GETSTRING");



            }
        });
    }


    void clearPref(){

        getContext().getSharedPreferences(Constants.sharedAppStat, Context.MODE_PRIVATE).edit().clear().apply();
        getContext().getSharedPreferences(Constants.sharedNet, Context.MODE_PRIVATE).edit().clear().apply();
        getContext().getSharedPreferences(Constants.sharedPref, Context.MODE_PRIVATE).edit().clear().apply();
        getContext().getSharedPreferences(Constants.stockCount, Context.MODE_PRIVATE).edit().clear().apply();
        //Cleared data


    }

}
