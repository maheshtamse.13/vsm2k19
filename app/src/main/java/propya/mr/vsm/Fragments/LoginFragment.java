package propya.mr.vsm.Fragments;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.heinrichreimersoftware.materialintro.app.SlideFragment;

import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.UI.AppStatus;

public class LoginFragment extends SlideFragment {
    
    View rootView;
    Button login;
    EditText username;
    EditText password;
    boolean loggedIn=false;


    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        username=(EditText)rootView.findViewById(R.id.username);
        password=(EditText)rootView.findViewById(R.id.password);


        login=(Button)rootView.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if((username.getText().toString()).equals("") || (password.getText().toString()).equals(""))
//                    loggedIn=false;
//                else
//                {loggedIn=true;
//                    updateNavigation();
//                    nextSlide();}

                if(username.getText().toString().equals("pulin")){
                    String pass =password.getText().toString();
                    if(pass.equals("vsm9102")){
                        GetData.setLogged(true);
                        GetData.setDay(1);
                        loggedIn = true;
                        GetData.setCash(AppStatus.getContext(),Constants.initailWorth);
                        updateNavigation();
                        nextSlide();
                    }else if(pass.equals("ppulin0299")){

                        GetData.setLogged(true);
                        GetData.setDay(0);
                        loggedIn = true;
                        GetData.setCash(getContext(),Constants.initailWorth);
                        updateNavigation();
                        nextSlide();

                    }else if(pass.equals("oculu1920")){

                        GetData.setLogged(true);
                        GetData.setDay(2);
                        loggedIn = true;
                        GetData.setCash(getContext(),Constants.initailWorth);
                        updateNavigation();
                        nextSlide();

                    }
                    else if(pass.equals("sptbI")){

                        GetData.setLogged(true);
                        GetData.setDay(4
                        );
                        loggedIn = true;
                        GetData.setCash(AppStatus.getContext(), Constants.initailWorth);
                        updateNavigation();
                        nextSlide();

                    }


                    else{
                        Snackbar.make(rootView,"Please Check Password",Snackbar.LENGTH_SHORT).show();

                    }



                }else{
                    Snackbar.make(rootView,"Please Check Username",Snackbar.LENGTH_SHORT).show();
                }



            }
        });
    }

    @Override
    public boolean canGoForward() {
        return loggedIn;
    }
}
