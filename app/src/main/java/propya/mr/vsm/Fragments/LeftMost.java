package propya.mr.vsm.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import propya.mr.vsm.Adapters.DisplayStocksAdapter;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.EachStock;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeftMost extends Fragment {

    View rootView;
    RecyclerView recyclerView;

    public LeftMost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.about_us, container, false);
        return rootView;
    }

//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
////        recyclerView = (RecyclerView)rootView.findViewById(R.id.teamRecycler);
//        ArrayList<EachStock> team = new ArrayList<>();
//        EachStock p = new EachStock("Pulin Prabhu");
//        p.setDesc("Executive Head");
//
//        EachStock abh = new EachStock("Abhishek Revadekar");
//        abh.setDesc("Creative & Technical Head");
//
//        EachStock romit = new EachStock("Romit Kankaria");
//        romit.setDesc("Marketing & PR Head");
//
//
//         EachStock m = new EachStock("Mahesh Tamse");
//        m.setDesc("Technical Head");
//
//        EachStock som = new EachStock("Somil Jain");
//        som.setDesc("Creative & Technical Head");
//
//        EachStock har = new EachStock("Harit Bandi");
//        har.setDesc("Marketing & PR Head");
//
//        EachStock ruch = new EachStock("Ruchi Gupte");
//        ruch.setDesc("Technical Head");
//
//        EachStock vrinda = new EachStock("Vrinda Bhatu");
//        vrinda.setDesc("Creative & Technical Head");
//
//        EachStock chinu = new EachStock("Chinmay Bajaj");
//        chinu.setDesc("Marketing & PR Head");
//
//        team.add(p);
//        team.add(abh);
//        team.add(romit);
//        team.add(m);
//        team.add(som);
//        team.add(har);
//        team.add(ruch);
//        team.add(vrinda);
//        team.add(chinu);
//
//
//        DisplayStocksAdapter adapter = new DisplayStocksAdapter(getContext(),team,rootView);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.setNestedScrollingEnabled(false);
//    }
}
