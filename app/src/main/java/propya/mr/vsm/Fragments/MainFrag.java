package propya.mr.vsm.Fragments;


import android.animation.Animator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import propya.mr.vsm.Activities.MainActivity;
import propya.mr.vsm.Adapters.DisplayStocksAdapter;
import propya.mr.vsm.Listeners.ViewLoaded;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.EachStock;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.Story.GetStory;
import propya.mr.vsm.UI.AnimateMiddle;
import propya.mr.vsm.UI.AppStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFrag extends AnimationMiddle {


    RecyclerView stockDisplay;

//    View pink;
    View mainView;
//    FloatingActionButton fab;
    DisplayStocksAdapter adapter;
    int round;


    public MainFrag() {
        // Required empty public constructor
    }




    @Override
    public void onResume() {
        super.onResume();
        AppStatus.currentContextPrimary = getContext();
        AppStatus.fragment = 1;
    }


    @Override
    public void onPause() {
        super.onPause();
        AppStatus.currentContextPrimary = null;
        AppStatus.fragment = -1;

        if(adapter != null){

            adapter.close();

        }


    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        clearCaps();

        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_main, container, false);
        Bundle b = getArguments();
//        setCapAvail(Constants.maxPerRound);
        if(b!=null){
            round = b.getInt("round",0);
        }else{
            round=0;
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        listener.viewHasLoaded(rootView);
        startTimer();
        Log.i("ANIMATE","Fro frag"+Integer.toString(rootView.getWidth()));
        Log.i("ANIMATE","callback triggered");
        WaitForBeacon.isScaning = false;
        viewSetter();
    }

    void viewSetter(){
        pink = rootView.findViewById(R.id.pinkLayer);
        stockDisplay = (RecyclerView)rootView.findViewById(R.id.stockRecycler);
        fab=MainActivity.getMiddleButton();
        adapter = new DisplayStocksAdapter(getContext(), GetStory.getStocks(GetData.getDay()),rootView);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        stockDisplay.setAdapter(adapter);
        stockDisplay.setLayoutManager(manager);

    }





    public View getView(){
        return rootView;
    }
    int time;

    void startTimer(){

        time = Constants.roundDuration;
        final CountDownTimer timer = new CountDownTimer(time*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time--;
//                if(MainActivity.timerDisplay!=null){
//                    MainActivity.timerDisplay.setTitle(Integer.toString(time));
//                }
                timer(Integer.toString(time));

            }

            @Override
            public void onFinish() {
                fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                AppStatus.roundTimerRunnig = false;
                GetData.setNetWorth();
                showLyrics();
                fab.setImageResource(R.drawable.ic_blur_on_black_24dp);
                GetData.setAppStat(2);
            }
        };
        timer.start();
        AppStatus.roundTimerRunnig = true;


    }

    private void showLyrics() {
        if(GetData.getDay()==0){
            try {
                MainActivity.revealAnim(MainActivity.getBeacon());
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final LyricsDisplay frag = new LyricsDisplay();
        ViewLoaded listener = new ViewLoaded() {
            @Override
            public void viewHasLoaded(View v) {

                Animator a = AnimateMiddle.getAnimatoin(v);
                a.start();
                a.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        frag.hidePink();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });



            }
        };

        Bundle b = new Bundle();
        b.putInt(Constants.roundNo,5);
        frag.setListener(listener);
        frag.setArguments(b);
try {
    ((AppCompatActivity)AppStatus.getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment,frag).commit();

}catch (Exception e){

}



    }

    private void timer(String time){

        long secs=Long.parseLong(time);


        BottomSheetForStock.setTimer((int) secs);

        try {


            if (secs < 6) {
                if (secs % 2 != 0)
                    fab.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                else
                    fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            }

            fab.setImageBitmap(textAsBitmap(time, 40, Color.WHITE));
        }catch (Exception e){
            
        }


    }



    //method to convert your text to image
    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }


    public static int getCountStock(String name){
        name = name.toLowerCase().replaceAll(" ","");
        return AppStatus.getContext().getSharedPreferences("cap",Context.MODE_PRIVATE).getInt(name,0);


    }


    public static int getCapAvail(){

        ArrayList<EachStock> stocks =  GetStory.getStocks(GetData.getDay());


        int total = 0;
        for(EachStock e:stocks){
            String name = e.getName().replace(" ","").toLowerCase();
            total+= AppStatus.getContext().getSharedPreferences("cap",Context.MODE_PRIVATE).getInt(name,0);
            Log.i("PULIN iter",e.getName()+"  "+name+" total "+total);
        }


       return Constants.maxPerRound() -total;

    }

//    ////TODO
//    public static void setCapAvail(int avail){
//       AppStatus.getContext().getSharedPreferences("cap", Context.MODE_PRIVATE).edit().putInt("cap",avail).apply();
//
//    }

    public static int getMaxAvail(String name){
        name=name.replaceAll(" ","").toLowerCase();
        int low = getCapAvail();
        Log.i("PULIN",name);
        Log.i("PULIN"," "+low);
        Log.i("PULIN"," "+getAvailFor(name));

        if(low>getAvailFor(name)){
            low = getAvailFor(name);
        }
        return low;
    }


    public static void setBoughtFor(int count,String name){
        name = name.toLowerCase().replaceAll(" ","");
        AppStatus.getContext().getSharedPreferences("cap", Context.MODE_PRIVATE).edit().putInt(name,count).apply();

    }
    public static int getAvailFor(String name){
        name = name.toLowerCase().replaceAll(" ","");
        return Constants.maxPerStock() -  AppStatus.getContext().getSharedPreferences("cap", Context.MODE_PRIVATE).getInt(name,0);//.apply();

    }



    public static void clearCaps(){
        AppStatus.getContext().getSharedPreferences("cap", Context.MODE_PRIVATE).edit().clear().apply();

    }

}
