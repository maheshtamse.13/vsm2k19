package propya.mr.vsm.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import propya.mr.vsm.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetString extends BottomSheetDialogFragment {

    View rootView;
    View.OnClickListener listener;
    public static String code;
    Button submit;
    EditText codeEdit;

    public GetString() {
        // Required empty public constructor
    }

    public void setParams(View.OnClickListener listener){
        this.listener = listener;

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_get_string, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewSetter();
    }




    private void viewSetter() {

        submit = (Button)rootView.findViewById(R.id.bottomSubmit);
        codeEdit = (EditText)rootView.findViewById(R.id.bottomCode);

        codeEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    listener.onClick(rootView);
                    return true;
                }
                return false;
            }
        });









        codeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                code = s.toString();
            }
        });

        submit.setOnClickListener(listener);


    }
}
