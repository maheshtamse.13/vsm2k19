package propya.mr.vsm.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import propya.mr.vsm.R;
import propya.mr.vsm.Story.EachStock;
import propya.mr.vsm.Story.GetData;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomSheetForStock extends BottomSheetDialogFragment {

    public View parentView;
    View rootView;
    View.OnClickListener listener;
    Button minus,plus,buy,sell;
    TextView name,pro,dumb,rate,max;
    static TextView timer;
    EditText units;
    public  int unitCount=0,position;
    public EachStock stock;
    static boolean isVisible = false;
    boolean isLast = false;


    public void hideCaps(){
        isLast = true;
    }

    public BottomSheetForStock() {
        // Required empty public constructor
    }

    public void setListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_bottom_sheet_for_stock, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewSetter();
    }

    private void viewSetter() {
        buy = (Button)rootView.findViewById(R.id.bottomBuy);
        plus = (Button)rootView.findViewById(R.id.bottomPlus);
        minus = (Button)rootView.findViewById(R.id.bottomMinus);
        name = (TextView)rootView.findViewById(R.id.bottomName);
        pro = (TextView)rootView.findViewById(R.id.bottomPro);
        dumb = (TextView)rootView.findViewById(R.id.bottomDumb);
        rate = (TextView)rootView.findViewById(R.id.rate);
        units = (EditText)rootView.findViewById(R.id.bottomUnits);
        buy.setOnClickListener(listener);
        sell = (Button)rootView.findViewById(R.id.bottomSell);
        max = (TextView)rootView.findViewById(R.id.bottomMax);
        timer = (TextView)rootView.findViewById(R.id.bottomTimer);
        if(isLast){
            max.setVisibility(View.GONE);
        }

        max.setText("Max stocks you can trade are : "+MainFrag.getMaxAvail(stock.getName()));

        unitCount=0;
        units.setText("0");


        units.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    unitCount = Integer.parseInt(s.toString());
                }catch (Exception e){

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                try{
                    rate.setVisibility(View.VISIBLE);
                    rate.setText(Integer.toString(GetData.getRate(stock.getName(),GetData.getRound()))+" x "+s.toString()+" = "+(GetData.getRate(stock.getName(),GetData.getRound())*Integer.parseInt(s.toString())));

                }catch(Exception e){

                }
                }
        });


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitCount++;
                units.setText(Integer.toString(unitCount));

            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(unitCount <=0){
                    Snackbar.make(parentView,"You already are poor",Snackbar.LENGTH_SHORT).show();
                    return;
                }
                unitCount--;
                units.setText(Integer.toString(unitCount));

            }
        });

        try {
            String tip = GetData.getProTip(getContext(),stock.getName().trim(),GetData.getRound());
            if(tip == null){
                pro.setVisibility(View.GONE);
            }else{
                pro.setText("Pro Tip: "+tip);
            }



        }catch (Exception e){
            pro.setVisibility(View.GONE);
        }
        try {
            String tip = GetData.getConTip(getContext(),stock.getName().trim(),GetData.getRound());
            if(tip == null){
                dumb.setVisibility(View.GONE);
            }else{
                dumb.setText("Caution: "+tip);
            }



        }catch (Exception e){
            dumb.setVisibility(View.GONE);
        }


        name.setText(stock.getName());

        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitCount*=-1;
                listener.onClick(v);
            }
        });


    }

    public static void setTimer(int time){
        if(!isVisible){
            return;
        }
        if(timer == null){
            return;
        }
        try{
            timer.setText(""+time+" secs");
        }catch (Exception e){
            try {
                timer.setVisibility(View.GONE);
            }catch (Exception e2){

            }
        }




    }


    @Override
    public void onResume() {
        super.onResume();
        isVisible  = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isVisible = false;
    }
}
