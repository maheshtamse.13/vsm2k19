package propya.mr.vsm.Fragments;


import android.net.LinkAddress;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import propya.mr.vsm.R;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.UI.AppStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class Stats extends Fragment {

    View day1,day2;
    LinearLayout trial;
    View rootView;
    int day;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        trial=rootView.findViewById(R.id.trial_desc);
        day1=rootView.findViewById(R.id.day1_desc);
        day2=rootView.findViewById(R.id.day2_desc);

        day= GetData.getDay();
        getCards(day);
    }

    public Stats() {
        // Required empty public constructor
    }



    @Override
    public void onResume() {
        super.onResume();
        AppStatus.currentContextPrimary = getContext();
        AppStatus.fragment = 9;
    }


    @Override
    public void onPause() {
        super.onPause();
        AppStatus.currentContextPrimary = null;
        AppStatus.fragment = -1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_stats, container, false);
        return rootView;
    }


    private void getCards(int day){
        if(day==0){
            trial.setVisibility(View.VISIBLE);
            day1.setVisibility(View.GONE);
            day2.setVisibility(View.GONE);
        }
        if(day==1){
            day1.setVisibility(View.VISIBLE);
            trial.setVisibility(View.GONE);
            day2.setVisibility(View.GONE);
        }
        if(day==2){
            day2.setVisibility(View.VISIBLE);
            day1.setVisibility(View.GONE);
            trial.setVisibility(View.GONE);
        }
    }
}
