package propya.mr.vsm.Fragments;


import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.utils.UrlBeaconUrlCompressor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import propya.mr.vsm.Activities.MainActivity;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.UI.AppStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class WaitForBeacon extends AnimationMiddle implements BeaconConsumer {

    //    View rootView;
    BeaconManager manager;
    Region region;
    TextView timeLeft;
    GetString getString;
    TextView aukat,profit,round;
    Button code;
    ImageView arrow;
    String timerStarted = "Timer has started";
    static boolean[] rounds = {false,false,false,false,false,false,false,false,false,false,false,false,false};
//13
    public static boolean isScaning = false;


    public WaitForBeacon() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_wait_for_beacon, container, false);
        pink = rootView.findViewById(R.id.pinkLayerBeacon);
        arrow = rootView.findViewById(R.id.arrow);
        pink.setVisibility(View.VISIBLE);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewSetter();



    }




    void bluetooth()
    {


        if(isScaning){
            return;
        }

        if(!AppStatus.beaconTimerRunnig){
            try {
                isScaning = true;
                isBluetoothEnabled();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        setTimer(11,10);

//        listen();

    }








//    void listen(){
//        AppStatus.beaconShouldScan = true;
//        MainActivity.bluetoothHelpers.enableScan();
//
//    }






    void viewSetter(){


        aukat = (TextView)rootView.findViewById(R.id.aukatRound);
        aukat.setText(Integer.toString(GetData.getNetWorth(GetData.getRound())));
        LinearLayout layout=(LinearLayout) rootView.findViewById(R.id.your_profit);
        round  = (TextView)rootView.findViewById(R.id.beaconRound);
        round.setVisibility(View.GONE);

        int r = GetData.getRound();

        profit = (TextView)rootView.findViewById(R.id.profitRound);

        if(r==0){
            //1st round
            layout.setVisibility(View.GONE);

        }else{
            layout.setVisibility(View.VISIBLE);
            int netWorth=GetData.getProfitRound(GetData.getRound());
            if(netWorth>0)
                arrow.setImageResource(R.drawable.ic_green);
            else if(netWorth<0)
                arrow.setImageResource(R.drawable.ic_red);
            else
                arrow.setImageResource(R.drawable.ic_blue);

            profit.setText(Integer.toString(netWorth));

        }

        round.setText("Round "+(r+1)+" starts in");
        code = (Button) rootView.findViewById(R.id.codeBeacon);

        final String scan = "Press to Start Scanning";
        final String manual = "Enter code manually";
        code.setText(scan);

//        if(isScaning){
//            code.setText(manual);
//        }


        code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(code.getText().toString().equals("Scanning ...")){
                    Snackbar.make(rootView,"Please wait till we try scanning",Snackbar.LENGTH_SHORT).show();
                    return;
                }

                if(code.getText().toString().equals(timerStarted)){
                    Snackbar.make(rootView,"Timer has already started",Snackbar.LENGTH_SHORT).show();
                    return;
                }

                if(code.getText().toString().equals(scan)){
                    try {
                        bluetooth();
                        code.setText("Scanning ...");
                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                if(code.getText().toString().equals(timerStarted)){
                                    return;
                                }
                                code.setText(manual);
                            }
                        },12000);
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(code.getText().toString().equals(manual)){
                    getCode();
                }


            }
        });



    }

    private void getCode() {
        getString = new GetString();
        getString.show(getChildFragmentManager(),"GETSTRING");
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GetData.isValidBeacon(GetString.code)) {

                    getTimer();
                    getString.dismiss();

                }
            }
        };
        getString.setParams(listener);


    }

    public int mappings(String time){

        HashMap<String,Integer> pass = new HashMap<>();
        pass.put("a",0);
        pass.put("b",1);
        pass.put("c",2);
        pass.put("d",3);
        pass.put("e",4);
        pass.put("f",5);
        pass.put("g",6);
        pass.put("h",7);
        pass.put("i",8);
        pass.put("j",9);



        int len = time.length();
        int no =0;
        for(int i=0;i<len;i++){

            no+=(pass.get(String.valueOf(time.charAt(i)))*Math.pow(10,len-i-1));

        }
        return no;

    }

    private void getTimer() {

        String time=GetString.code;
        try{

            String[] hm = time.split(":");

            int hrs =mappings(hm[0]);
            int min =mappings(hm[2]);
            setTimer(hrs,min);
            stopScanning();
        }catch (Exception e){
            Snackbar.make(rootView,"Enter Valid Code",Snackbar.LENGTH_SHORT).show();
        }


    }


    boolean isBluetoothEnabled() throws Exception{

        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(!adapter.isEnabled()){
        //    Toast.makeText(getContext(), "Trying to enable ", Toast.LENGTH_SHORT).show();
            adapter.enable();

            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {

                    listenForBeacon();
                }
            }, 5000);


        }else{
            listenForBeacon();
        }


        return false;
    }


    void listenForBeacon(){

        try{


            manager = BeaconManager.getInstanceForApplication(getContext());
            manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_URL_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_TLM_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT));
            manager.setBackgroundMode(false);
//            manager.setForegroundBetweenScanPeriod(1000);
            manager.bind(this);
        }
        catch (Exception e){


        }

    }


    @Override
    public void onBeaconServiceConnect() {
        Log.i("BEACIN","Connected");

        ArrayList<Identifier> identifiers = new ArrayList<>();
        identifiers.add(null);

        region = new Region("All",identifiers);

        try {
            manager.startRangingBeaconsInRegion(region);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


        RangeNotifier rangeNotifier = new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                Log.i("BEACIN","did range");
                Log.i("BEACIN",Integer.toString(collection.size()));
                for(Beacon c : collection){
                    Log.i("BEACIN",c.toString());
                    Log.i("BEACIN code",Integer.toString(c.getBeaconTypeCode()));
                    if (c.getServiceUuid() == 0xfeaa && c.getBeaconTypeCode() == 0x10) {
                        Log.i("BEACIN","edy url agaya");
                        // This is a Eddystone-URL frame
                        String url = UrlBeaconUrlCompressor.uncompress(c.getId1().toByteArray());
                        Log.d("BEACIN", "I see a beacon transmitting a url: " + url +
                                " approximately " + c.getDistance() + " meters away.");
                        if(isVsmUrl(url)){
                            try {
                                int r =Integer.parseInt(url.split("\\.")[3]);
                                if(rounds[r]){
                                    return;
                                }
                                rounds[r]=true;
                            }catch (Exception e){
                                return;
                            }


                            startTimer(url);
                        }
                    }
                }
            }
        };
        manager.addRangeNotifier(rangeNotifier);
    }

    private void startTimer(String url) {

        try {


            String u[] = url.split("\\.");
            int hr = Integer.parseInt(u[1]);
            int min = Integer.parseInt(u[2]);

  //          Toast.makeText(getContext(), "The round starts at " + u[1] + ":" + u[2], Toast.LENGTH_SHORT).show();
            stopScanning();
            setTimer(hr,min);

        }catch (Exception e){

        }

    }

    @Override
    public Context getApplicationContext() {
        if(getContext() != null){
            return getContext();
        }
        return getActivity().getApplicationContext();
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {
        getContext().unbindService(serviceConnection);
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return getContext().bindService(intent,serviceConnection,i);
    }


    @Override
    public void onPause() {
        super.onPause();
        AppStatus.fragment =-1;
        Log.i("BEACIN","on pause");

        if(manager == null){
            return;
        }

        if(manager.isBound(this)) {
//            isScaning = false;
            manager.unbind(this);
            try {
                manager.stopRangingBeaconsInRegion(region);
            } catch (RemoteException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    boolean isVsmUrl(String url){

        try {
            String s = url.split("//")[1];
            s = s.split("\\.")[0];
            return s.equals("vsm");
        }catch (Exception e){
            return false;
        }




    }


    void stopScanning(){
        if(manager != null){
            if(manager.isBound(this)){
                manager.unbind(this);
            }
            try {
                manager.stopRangingBeaconsInRegion(region);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        BluetoothAdapter.getDefaultAdapter().disable();



    }

    void setTimer(int hr , int min){
//        isScaning = false;

        Log.i("BEACIN","SETTING timer");
        Date date = Calendar.getInstance().getTime();
//        DateFormat format = DateForm
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy ,hh:mm:ss");
        Time time = new Time();
        time.setToNow();
        dateFormat.setTimeZone(TimeZone.getDefault());


        String timeCurrent = dateFormat.format(date);

        String reminder = timeCurrent.split(",")[0];
        reminder+=String.format(",%d:%d:00",hr,min);



        long diff =0;
        try {

            Log.i("BEACIN",timeCurrent+ dateFormat.parse(timeCurrent).getTime());
            Log.i("BEACIN",reminder+dateFormat.parse(reminder).getTime());



            diff = dateFormat.parse(reminder).getTime();
            diff-= dateFormat.parse(timeCurrent).getTime();
        }catch (Exception e){
            Log.i("BEACIN",Long.toString(diff));
        }

        Log.i("BEACIN","diff 0"+Long.toString(diff));
        if(diff == 0){

            return;
        }
//
//        CountDownTimer timer = new CountDownTimer(diff,1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                MainActivity.timerDisplay.setTitle(String.format("Time left is %d",millisUntilFinished/1000));
//            }
//
//            @Override
//            public void onFinish() {
//                GetData.incrementRound();
//            }
//        };
//        timer.start();

        code.setText(timerStarted);
        MainActivity.timer.roundStartsIn(diff,"next");


    }

    @Override
    public void onResume() {
        super.onResume();
        AppStatus.fragment = 10;
//        viewSetter();
    }


}
