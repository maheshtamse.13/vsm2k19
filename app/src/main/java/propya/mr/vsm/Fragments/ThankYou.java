package propya.mr.vsm.Fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import propya.mr.vsm.Activities.MainActivity;
import propya.mr.vsm.Activities.MainIntroActivity;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.UI.AppStatus;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThankYou extends AnimationMiddle {
    TextView netWorth;

    public ThankYou() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        AppStatus.fragment = 7;
        AppStatus.currentContextPrimary= getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_thank_you, container, false);
        pink = rootView.findViewById(R.id.thanksPink);
        netWorth = (TextView)rootView.findViewById(R.id.netThanks);
        pink.setVisibility(View.VISIBLE);
        TextView thank=rootView.findViewById(R.id.thanks);
        thank.setTypeface(Typeface.createFromAsset(getContext().getAssets(),"fonts/edo.ttf"));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                clearSharedPref();
            }
        },5000);
        netWorth.setText("Your Net Worth is "+GetData.getNetWorth(GetData.getRound()));
    }

    void clearSharedPref(){


        int day = GetData.getDay();

        boolean needToSet = false;

        if(day == 0 ){
            needToSet = true;
        }


        AppStatus.getContext().getSharedPreferences(Constants.sharedAppStat, Context.MODE_PRIVATE).edit().clear().commit();
        AppStatus.getContext().getSharedPreferences(Constants.sharedNet, Context.MODE_PRIVATE).edit().clear().commit();
        AppStatus.getContext().getSharedPreferences(Constants.sharedPref, Context.MODE_PRIVATE).edit().clear().commit();
        AppStatus.getContext().getSharedPreferences(Constants.stockCount, Context.MODE_PRIVATE).edit().clear().commit();

        //Cleared data

        if(needToSet){
            ////TODO use set day and set money if round is
            GetData.setCash(AppStatus.getContext(),Constants.initailWorth);
            GetData.setDay(2);
            GetData.setLogged(true);
            try {
                MainActivity.revealAnim(new WaitForBeacon());
            } catch (Exception e) {
                e.printStackTrace();
            }


        }else{
            ////TODO close app
//            Intent i = new Intent(AppStatus.getContext(), MainIntroActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(i);

        }






    }

}
