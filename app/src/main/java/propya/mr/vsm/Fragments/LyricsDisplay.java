package propya.mr.vsm.Fragments;


import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import propya.mr.vsm.Activities.MainActivity;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.GetData;

/**
 * A simple {@link Fragment} subclass.
 */
public class LyricsDisplay extends AnimationMiddle {

    //    View rootView,pink;
//    ViewLoaded listener;
    TextView lyric;
    //    FloatingActionButton fab;
    AssetManager assetManager;
    Button next;
    String[] fonts={"budmo.TTF","depress.ttf","edo.ttf","fourhand.ttf","sunspire.ttf"};

    public LyricsDisplay() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_lyrics_display, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewSetter();
//        if(listener!=null){
//            listener.viewHasLoaded(rootView);
//        }


    }

//    public void hidePink(){
//        pink.animate().alpha(0).setDuration(500);
//    }
//
//    public void setListener(ViewLoaded listener){
//        this.listener = listener;
//    }



    void viewSetter(){
        pink = rootView.findViewById(R.id.pinkLayer);
        fab=MainActivity.getMiddleButton();
        pink.setVisibility(View.VISIBLE);
        assetManager=getContext().getAssets();
        lyric=(TextView)rootView.findViewById(R.id.lyric);
        int index=(int)(Math.random()*fonts.length);

        lyric.setTypeface(Typeface.createFromAsset(assetManager,"fonts/"+fonts[index]));


        getLyrics();


        next = (Button)rootView.findViewById(R.id.lyricsNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();

            }
        });


//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getCode();
//            }
//        });


    }


    void getCode(){

        final GetString getString =  new GetString();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getString.dismiss();
                String code = GetString.code;

                String pass = Constants.getPassNextRound(GetData.getRound());
                if(pass.equals(code)){

//                    ((AppCompatActivity)AppStatus.getContext()).getSupportFragmentManager().beginTransaction().addToBackStack(null)
//                            .replace(R.id.fragment,new WaitForBeacon()).commit();
                    try {
                        MainActivity.revealAnim(new WaitForBeacon());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    GetData.setAppStat(0);

                }else{
                    Snackbar.make(rootView,"Please Enter Valid code",Snackbar.LENGTH_SHORT).show();
                }



            }
        };

        getString.setParams(listener);
        getString.show(getChildFragmentManager(),"GetString");


    }



    void setLyric(String profit){
        int round = GetData.getRound();
        double profitAvail = GetData.getProfitRound(round);
        String ranges[] = profit.split(":");
        ArrayList<String> r1 = new ArrayList<>();
        ArrayList<String> r2 = new ArrayList<>();
        ArrayList<String> r3 = new ArrayList<>();
        ArrayList<String> r4 = new ArrayList<>();
        ArrayList<String> r5 = new ArrayList<>();


        r1.add("Koi Mil Gaya");
        r1.add("Deewana hain dekho");
        r1.add("Tunak Tunak Tun ");
        r1.add("Kaho na pyaar hain");


        r2.add("Dus bahaane");
        r2.add("Taare gin gin");
        r2.add("Dhoom Machale");
        r2.add("Kajra re");

        r3.add("Zoobi Doobi");
        r3.add("You're my love");
        r3.add("All hot girls put your hands up and say");
        r3.add("Jhoom Barabar");

        r4.add("Chamak Challo");
        r4.add("Badtameez Dil");
//        r4.add("Dhunki");
        r4.add("Criminal raone");
        r4.add("Sadda Haaq");

        r5.add("Apna Time Ayega");
        r5.add("Zingaat");
        r5.add("Ishaare Tere");
        r5.add("Swag see swaagat");

        ArrayList<String> r  =null;
        switch (round){
            case 1:
                r=r1;
                break;
            case 2:
                r=r2;
                break;
            case 3:
                r=r3;
                break;
            case 4:
                r=r4;
                break;
            case 5:
                r=r5;
                break;
            default:
                r=r5;
        }
        String result;
        int[] rates = {Integer.parseInt(ranges[0]),Integer.parseInt(ranges[1]),Integer.parseInt(ranges[2])};
        if(profitAvail >=rates[0])
        {
            result =  r.get(0);
        }else if(profitAvail >= rates[1]){
            result =  r.get(1);
        }else if(profitAvail >= rates[2]){
            result =  r.get(2);
        }else{
            result =  r.get(3);
        }

        lyric.setText(result);


    }


    void getLyrics() {

        int day = GetData.getDay();
        int round = GetData.getRound();

        if(day==0){
            switch (round){

                case 1:
                    setLyric("435:232:0");
                    break;
                case 2:
                    setLyric("440:26:0");
                    break;
                case 3:
                    setLyric("600:340:0");
                    break;
                case 4:
                    setLyric("420:230:0");
                    break;
                case 5:
                    setLyric("2100:1800:1000");
                    break;


            }
            return;
        }



        if(day ==1){
        switch (round) {
            case 1:
                setLyric("435:232:0");
                break;
            case 2:
                setLyric("440:26:0");
                break;
            case 3:
                setLyric("600:340:0");
                break;
            case 4:
                setLyric("420:230:0");
                break;
            case 5:
                setLyric("2100:1800:1000");
                break;


        }

            return;


        }

        if(day ==2){
        switch (round) {
            case 1:
                setLyric("555:324:0");
                break;
            case 2:
                setLyric("1400:700:0");
                break;
            case 3:
                setLyric("1200:640:0");
                break;
            case 4:
                setLyric("975:520:0");
                break;
            case 5:
                setLyric("2375:1425:855");
                break;


        }

            return;


        }

        setLyric("435:232:0");


    }
}
