package propya.mr.vsm.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.View;

import propya.mr.vsm.Listeners.ViewLoaded;

public class AnimationMiddle extends Fragment {
    View rootView,pink;
    ViewLoaded listener;
    FloatingActionButton fab;



    public void hidePink(){
        if(pink == null){
            return;
        }
        pink.animate().alpha(0).setDuration(500);
    }

    public void setListener(ViewLoaded listener){
        this.listener = listener;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(rootView!=null && listener!=null){
            listener.viewHasLoaded(rootView);
        }else{
            if(pink!=null){
                pink.setVisibility(View.GONE);
            }
        }

    }
}
