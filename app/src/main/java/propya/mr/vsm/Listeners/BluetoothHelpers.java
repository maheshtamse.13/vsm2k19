package propya.mr.vsm.Listeners;

import org.altbeacon.beacon.BeaconConsumer;

public interface BluetoothHelpers extends BeaconConsumer {

    public void enableScan();

    public void stopScan();

    public void disableBluetooth();

    public void listenForBeacons();

    public void setTimer(int hh,int mm);

    public boolean isListening();

}
