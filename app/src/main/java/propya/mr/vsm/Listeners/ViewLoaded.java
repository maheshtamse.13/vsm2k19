package propya.mr.vsm.Listeners;

import android.view.View;

public interface ViewLoaded {

    public void viewHasLoaded(View v);

}
