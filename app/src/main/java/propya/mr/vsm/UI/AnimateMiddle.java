package propya.mr.vsm.UI;

import android.animation.Animator;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;

public class AnimateMiddle {

    public static  int cx,cy;
    public static Context c;



    public static Animator getAnimatoin( View myView){
        if(myView == null){
            return null;
        }



        int size=(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,28,c.getResources().getDisplayMetrics());


        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(cx, cy);
//        Log.i("ANIMATE"," left"+Integer.toString(dim[0]));
//        Log.i("ANIMATE"," right"+Integer.toString(dim[1]));
//        Log.i("ANIMATE"," size"+Integer.toString(size));


        // create the animator for this view (the start radius is zero)

        Animator anim = ViewAnimationUtils.createCircularReveal(myView,cx+size/2,cy+size/2, size, finalRadius);
        anim.setDuration(anim.getDuration()*2);
        return anim;
    }


}
