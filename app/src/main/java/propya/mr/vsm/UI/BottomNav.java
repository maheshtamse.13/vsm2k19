package propya.mr.vsm.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.design.widget.BottomNavigationView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import propya.mr.vsm.R;

public class BottomNav extends BottomNavigationView {


    Path path= new Path();
    Paint paint = new Paint();
    float radiusFAB=1;
    Context c;
    int width,height;

    public BottomNav(Context context) {
        super(context);
        this.c = context;
//        drawPath();
    }



    public BottomNav(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.c = context;
//        drawPath();
    }

    public BottomNav(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.c = context;
//        drawPath();
    }

    void setFabDp(int d){
        radiusFAB = getPixels(d);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = widthMeasureSpec;
        Log.i("BOTTOM","measeured width "+Integer.toString(widthMeasureSpec));
        height = heightMeasureSpec;
        Log.i("BOTTOM","on draw");
        drawPath();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i("BOTTOM", "on draw trying");
//        width = getWidth();
//        height = getHeight();

        canvas.drawPath(path, paint);
//        canvas.drawPaint(paint);

        Path p =new Path();
        p.moveTo(0,0);
        p.lineTo(50,50);
        p.lineTo(150,50);
        p.lineTo(width,height);


//        p.addRect(20,20,80,80,null);
//        canvas.drawLine(0,0,50,50,paint);
        Log.i("BOTTOM","before drawing"+path.toString());

        canvas.drawPath(p,paint);
        Log.i("BOTTOM","after drawing");
    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        this.width = w;
//        this.height = h;
//        drawPath();
//    }



    //00        10
    //01        11

    void drawPath(){
        Log.i("BOTTOM","func called");
//        width = getWidth();
//        height = getHeight();
        setWillNotDraw(false);
  //      Toast.makeText(c, "Drawing ", Toast.LENGTH_SHORT).show();
        Log.i("BOTTOM",Float.toString(width));
//        paint.setColor(getResources().getColor(R.color.colorAccent));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        paint.setColor(Color.YELLOW);

        float centre = width/(float)2;
        float offSet = radiusFAB+getPixels(16);

//        path.reset();


        path.moveTo(0,0);//at top left

//        path.lineTo(0+(centre - offSet),0);//before curve starts
        //reached start of curve

//        path.lineTo(centre+offSet,0);


        //reached end of curve
//        path.lineTo(width,0);//after ending curve
//        path.lineTo(width,height);
//        path.lineTo(0,height);
//        path.lineTo(0,0);
//        path.close();


        path.lineTo(width,height);

        Log.i("BOTTOM","drawing end");
    }

    float getPixels(int dp){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,c.getResources().getDisplayMetrics());
    }

}
