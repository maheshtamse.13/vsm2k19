package propya.mr.vsm.UI;

import android.content.Context;
import android.view.MenuItem;

public class AppStatus {

    public static int fragment = -1;


    public static boolean roundTimerRunnig  = false;

    public static boolean beaconTimerRunnig  = false;

    public static boolean beaconShouldScan = false;


    public static Context currentContextSecondary = null;
    public static Context currentContextPrimary = null;

    public static Context getContext(){
        if(currentContextPrimary!=null){
            return currentContextPrimary;
        }

        return currentContextSecondary;


    }


    public static MenuItem topMenu = null;

}
