package propya.mr.vsm.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import propya.mr.vsm.Fragments.BottomSheetForStock;
import propya.mr.vsm.Fragments.MainFrag;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.EachStock;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.Story.GetStory;
import propya.mr.vsm.UI.AppStatus;

public class DisplayStocksAdapter extends RecyclerView.Adapter<DisplayStocksAdapter.ViewHolder> {

    Context c;
    ArrayList<EachStock> stocks;
    View rootView;
    BottomSheetForStock bottomSheet;
    boolean isLastRound = false;


    public DisplayStocksAdapter(Context c, ArrayList<EachStock> stocks,View rootView){
        this.c = c;
        this.stocks = stocks;
        this.rootView = rootView;
        int round = GetData.getRound();

        if(Constants.getMaxRounds(GetData.getDay()) == round ){

            if(GetData.getDay()!=2){
                isLastRound = true;
            }

        }

    }

    public void close(){

        if(bottomSheet!=null){

            bottomSheet.dismiss();

        }


    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(c).inflate(R.layout.adapter_display_stock,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        String name =stocks.get(position).getName();
        holder.name.setText(name);
        int count = GetData.getStockCount(stocks.get(position).getName(),c);
        holder.count.setText(Integer.toString(count));
        holder.root.setTag(position);

        int round = GetData.getRound();

        int currentPrice = GetData.getRate(name, round);
        holder.price.setText(Integer.toString(currentPrice));
        if(round ==1){
            holder.profit.setVisibility(View.GONE);
            holder.stat.setVisibility(View.GONE);

        }else{
            int prev = GetData.getRate(name,round-1);

            if(currentPrice>prev){
                //profit
                holder.profit.setText(""+(currentPrice-prev));

            }else{
                //loss
                holder.stat.setImageBitmap(getBitmap(R.drawable.ic_red));
                holder.profit.setText(""+(prev-currentPrice));

            }




        }



        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStock((int)v.getTag());
            }
        });

        if(GetStory.desc(name)!=null){
            holder.desc.setText(GetStory.desc(name));
        }


        int resId = GetData.getImageId(name);
//        resId = R.drawable.logo_hulk;
        Log.i("ResId",Integer.toString(resId));
        try {
            if (resId != -1) {

                Bitmap b = BitmapFactory.decodeResource(c.getResources(), resId);

                if (b == null) {

                    b = getBitmap(resId);

//                Toast.makeText(c, "Res found but chu", Toast.LENGTH_SHORT).show();
                }
                holder.image.setImageBitmap(b);


            }
        }catch (Exception e){

        }
    }


    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = c.getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void handleStock(int position) {

        bottomSheet = new BottomSheetForStock();



        bottomSheet.unitCount = stocks.get(position).getQuantity();
        bottomSheet.position = position;
        bottomSheet.stock = stocks.get(position);
        bottomSheet.parentView = rootView;
        if(isLastRound){
            bottomSheet.hideCaps();
        }






        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
      //          Toast.makeText(c, "Count "+Integer.toString(bottomSheet.unitCount), Toast.LENGTH_SHORT).show();
                stocks.get(bottomSheet.position).setQuantity(bottomSheet.unitCount);

                bottomSheet.dismiss();

                int total  =bottomSheet.unitCount;



                //check if has enough money

                String stock = stocks.get(bottomSheet.position).getName();
                stock = stock.toLowerCase();

                int price= GetData.getRate(stock,GetData.getRound());
                int cost = price*total;

                int limit = MainFrag.getAvailFor(stock);

                if(total<0){
                    //sell
                    //take care total is negative
                    total*=-1;
                    cost*=-1;



                    int avail = GetData.getStockCount(stock,AppStatus.getContext());
                    if((avail) < total){
                        Snackbar.make(rootView,"You don't have enough stocks to sell ",Snackbar.LENGTH_SHORT).show();
                        //for total not round specific
                        return;
                    }

                    if((limit != Constants.maxPerStock())){
                        // has bought
                        int x = MainFrag.getCountStock(stock);
                        if(total>=x){
                            x=0;
                        }
                        else {
                            x = x - total;
                        }
                        MainFrag.setBoughtFor(x,stock);

                    }


                    GetData.setStockCount(stock,avail-total,c);
                    GetData.setCash(AppStatus.getContext(),GetData.getAvailCash(AppStatus.getContext())+cost);
                    notifyItemChanged(bottomSheet.position);




                    return;
                }

//buying


                if(GetData.getAvailCash(AppStatus.getContext())   > (cost)){

                    if((MainFrag.getMaxAvail(stock)>=total)){
                        MainFrag.setBoughtFor(MainFrag.getCountStock(stock)+total,stock);
                    }else{
                        if(!isLastRound){
                            Snackbar.make(rootView,"You have exceeded the cap",Snackbar.LENGTH_SHORT).show();
                            return;
                        }

                    }



                    GetData.setStockCount(stock,GetData.getStockCount(stock,AppStatus.getContext())+total,c);
                    GetData.setCash(AppStatus.getContext(),GetData.getAvailCash(AppStatus.getContext())-cost);
                    notifyItemChanged(bottomSheet.position);
                }else{
                    Snackbar.make(rootView,"You don't have enough money ",Snackbar.LENGTH_SHORT).show();
                }








            }
        };
        bottomSheet.setListener(listener);


        bottomSheet.show(((AppCompatActivity)c).getSupportFragmentManager(),"Bottom");



    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name,count,price,desc,profit;
        View root;
        ImageView image,stat;


        ViewHolder(View itemView){
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.adapterName);
            count = (TextView)itemView.findViewById(R.id.adapterCount);
            root = itemView.findViewById(R.id.stockRoot);
            image = (ImageView)itemView.findViewById(R.id.adapterImageView);
            price = (TextView)itemView.findViewById(R.id.adapterPrice);
            profit = (TextView)itemView.findViewById(R.id.adapterProfit);
            desc = (TextView)itemView.findViewById(R.id.descAdapter);
            stat = (ImageView)itemView.findViewById(R.id.adapterStat);

        }
    }
}
