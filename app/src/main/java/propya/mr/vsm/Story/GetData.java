package propya.mr.vsm.Story;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import propya.mr.vsm.Activities.MainActivity;
import propya.mr.vsm.UI.AppStatus;

public class GetData {

    public static void setAppStat(int stat){
        AppStatus.getContext().getSharedPreferences(Constants.sharedAppStat,Context.MODE_PRIVATE).edit().putInt(Constants.sharedAppStat,stat).apply();


    }


    public static int getAvailCash(Context c){
        int cash;
        SharedPreferences preferences = c.getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE);
        cash = preferences.getInt(Constants.playerMoney,0);
        return cash;
    }

    public static void setCash(Context c,int cash){

        SharedPreferences preferences = c.getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE);
        preferences.edit().putInt(Constants.playerMoney,cash).apply();
        setCashMenu();
    }


    public static void setCashMenu(){


        int cash = getAvailCash(AppStatus.getContext());
        MainActivity.timerDisplay.setTitle("Cash "+Integer.toString(cash));

    }


    public static String getProTip(Context c,String stockName,int round){
        stockName = stockName.replaceAll(" ","");
        String find = stockName.toLowerCase()+"_pro_"+Integer.toString(round);
        int id = getStringID(c,find);
        if(id ==-1 || id ==0){
//            Toast.makeText(c, "Error In getting pro tip call volunteer", Toast.LENGTH_SHORT).show();
            return null;
        }

        return c.getResources().getString(id);

    }

    public static String getConTip(Context c,String stockName,int round){
        stockName = stockName.replaceAll(" ","");
        String find = stockName.toLowerCase()+"_con_"+Integer.toString(round);
        int id = getStringID(c,find);
        if(id ==-1 || id ==0){
//            Toast.makeText(c, "Error In getting pro tip call volunteer", Toast.LENGTH_SHORT).show();
            return null;
        }

        return c.getResources().getString(id);

    }



    public static int getRate(String stockName,int round){
        stockName = stockName.replaceAll(" ","");

        Context c = AppStatus.getContext();
        stockName = stockName.toLowerCase();

        int id = getArrayID(c,stockName+"_rates");
        if(id ==-1 || id ==0){
            Toast.makeText(c, "Error In getting rate call volunteer", Toast.LENGTH_SHORT).show();
            return 0;
        }
        int[] rates = c.getResources().getIntArray(id);
        return rates[round-1];
    }


    private static int getArrayID(Context c, String stock){
        stock = stock.replaceAll(" ","");


        try{
            return  c.getResources().getIdentifier(stock,"array",c.getPackageName());

        }catch (Exception e){
            return -1;
        }


    }
    private static int getStringID(Context c, String stock){
        stock = stock.replaceAll(" ","");
        try{
            return  c.getResources().getIdentifier(stock,"string",c.getPackageName());

        }catch (Exception e){
            return -1;
        }

    }

    public static int getStockCount(String name,Context c){
        name = name.replaceAll(" ","");
        name=name.toLowerCase();
        Log.i("BEACIN",name);
        int count = 0;
        count = c.getSharedPreferences(Constants.stockCount,Context.MODE_PRIVATE).getInt(name,0);
        return count;
    }


    public static void setStockCount(String name,int count,Context c){
        name = name.replaceAll(" ","");
        name=name.toLowerCase();
        c.getSharedPreferences(Constants.stockCount,Context.MODE_PRIVATE).edit().putInt(name,count).apply();

    }


    public static String getLyrics(Context c,int day,int round){

        String lyrics = null;

        lyrics ="jxfhfxjfx mmhsdfjhds mhajkdjkd";



        return lyrics;
    }

    public static void setRound(int round){

        AppStatus.getContext().getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE).edit().putInt(Constants.roundNo,round).apply();

    }

    public static int getDay(){

        return  AppStatus.getContext().getSharedPreferences(Constants.sharedAppStat,Context.MODE_PRIVATE).getInt(Constants.dayNo,0);
    }


    public static void setDay(int day){

        AppStatus.getContext().getSharedPreferences(Constants.sharedAppStat,Context.MODE_PRIVATE).edit().putInt(Constants.dayNo,day).apply();

    }



    public static boolean incrementRound(){
        ////TODO validation of round no
        int round = getRound();
        int day = getDay();
        int max = Constants.getMaxRounds(day);

        if(max == round){
            return false;
        }
        else {
            setRound(round+1);
            return true;
            ////TODO clear all shared pref and redirect to login page

        }







    }

    public static int getRound(){




        return  AppStatus.getContext().getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE).getInt(Constants.roundNo,0);

//        return 1;


    }


    public static int getImageId(String company){
        company = company.replaceAll(" ","");
        company = company.toLowerCase();

        Context c = AppStatus.getContext();

        return c.getResources().getIdentifier("logo_"+company,"drawable",c.getPackageName());

    }


    public static int getNetWorth(int round){

        String identifier = String.format("net_worth_%d",round);

        return AppStatus.getContext().getSharedPreferences(Constants.sharedNet,Context.MODE_PRIVATE).getInt(identifier,Constants.initailWorth);
    }

    public static void setNetWorth(){


        int round = GetData.getRound();
        String identifier = String.format("net_worth_%d",round);

        ArrayList<EachStock> stocks = GetStory.getStocks(getDay());
        int worth = 0;
        for(EachStock e : stocks){
            String name = e.getName();
            name = name.replaceAll(" ","");
            name = name.toLowerCase();
            int rate = getRate(name,round+1);
            int count = getStockCount(name,AppStatus.getContext());
            worth+=rate*count;

            Log.i("BEACIN",name+" "+worth+"  rate "+rate+" count "+count);


        }
        AppStatus.getContext().getSharedPreferences(Constants.sharedNet,Context.MODE_PRIVATE).edit()
                .putInt(identifier,worth+GetData.getAvailCash(AppStatus.getContext())).apply();
        //pro tip... use wrong spellings in log so u can easily find them later
        Log.i("WORETH set",identifier+"   :"+worth);
    }


    public static boolean canPlayRound(int round){

        String identifier =  String.format("net_worth_%d",round);
        int result = AppStatus.getContext().getSharedPreferences(Constants.sharedNet,Context.MODE_PRIVATE).getInt(identifier,-1);
        if(result != -1){
            return true;
        }
        return false;
    }

    public static int getProfitRound(int round){
        Log.i("RoundC"," "+round);
        return getNetWorth(round)-getNetWorth(round-1);

    }

    public static boolean isValidBeacon(String pass){

//        if(pass.contains("abhi")||pass.contains("bobo")||pass.contains("propya")){
//            return true;
//        }
        String[] data = pass.split(":");
        if(data.length !=3){
            return false;
        }
        if(data[1].equals(Constants.getBeaconPass())){
            return true;
        }


        return false;

    }


    public static boolean isLoggedIn(){

        return AppStatus.getContext().getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE).getBoolean(Constants.isLogged,false);

//        return false;
    }

    public static void setLogged(boolean isLogged){

        AppStatus.getContext().getSharedPreferences(Constants.sharedPref,Context.MODE_PRIVATE).edit().putBoolean(Constants.isLogged,isLogged).apply();

    }


    public static void handleMerger(String parent,String child,int round){
        //Has not been checked
        int pRate = getRate(parent,round);
        int cRate = getRate(child,round);

        double ratio = cRate/(double)pRate;

        double stockReward;// =  Math.floor(ratio);
        int cashReward;// = (int) Math.ceil(ratio-stockReward);


        stockReward = ratio*getStockCount(child,AppStatus.getContext());

        double cashRatio = stockReward-Math.floor(stockReward);


        int stockInt = (int)Math.floor(stockReward);

        cashReward =(int)Math.ceil(cashRatio*pRate);
        setStockCount(parent,stockInt+getStockCount(parent,AppStatus.getContext()),AppStatus.getContext());
        setStockCount(child,0,AppStatus.getContext());

        setCash(AppStatus.getContext(),getAvailCash(AppStatus.getContext())+cashReward);


    }



}
