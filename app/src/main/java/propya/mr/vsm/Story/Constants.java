package propya.mr.vsm.Story;

import propya.mr.vsm.UI.AppStatus;

public class Constants {

    public static final String sharedPref = "userDetails";
    public static final String isLogged = "loginStat";
    public static final String playerMoney = "cashAvail";
    public static final String stockCount = "stockDetails";
    public static final String roundNo = "roundNo";
    public static final String dayNo = "dayNo";
    public static final int roundDuration = 60;

    public static final String sharedNet = "userNetWorth";

    public static final String sharedAppStat = "roundProgress";

    public static final int initailWorth =10000;

    public static int maxPerRound(){
        int round = GetData.getRound();
        if(round==5){
            return 100;
        }

        return 60;
    }

    public static int maxPerStock(){
        int round = GetData.getRound();
        if(round==5){
            return 30;
        }

        return 20;
    }
//    = 20;


    public static String getPassNextRound(int round){

        String[] pass = {"wontshowup","pokemon","hcverma","oswald","popeye","apnatimeaagaya" ,"samosa", "chicken", "dodo", "oreo", "pulin", "cid", "cuv"};
        return pass[round];

    }


    public static int getMaxRounds(int day){
        switch (day){
            case 0:
                return 3;
            case 1:
                return 5;
            case 2:
                return 5;
            case 4:
                return 4;


        }


        return 0;
    }

    public static String getBeaconPass(){

        String[] pass={
                "ben10","pokemon","teen titans","scoobydoo","digimon","lonney","dragon","romit","oneplus","chair","shoes","iphone","somil","television"
        };

        int round = GetData.getRound();

        return pass[round];
    }


}
