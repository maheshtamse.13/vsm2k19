package propya.mr.vsm.Story;

import java.util.ArrayList;

import propya.mr.vsm.R;

public class GetStory {



    public static double getProfit(int day,int round){

        double profit ;

        if(day == 1){
            profit = Day1.getProfits().get(round);
        }else{
            profit = Day2.getProfits().get(round);
        }

        return profit;

    }

    public static ArrayList<EachStock> getStocks(int day){
        ArrayList<EachStock> stocks = new ArrayList<>();
        if(day==0){
            stocks.add(new EachStock("Iron Man"));
            stocks.add(new EachStock("Captain America"));
//            stocks.add(new EachStock("Hulk"));
//            stocks.add(new EachStock("Thor"));
//            stocks.add(new EachStock("Hawkeye"));
//            stocks.add(new EachStock("Black Widow"));
            stocks.add(new EachStock("Thanos"));
        }
        else if(day==1){

            int round = GetData.getRound();


//            stocks.add(new EachStock("Google"));//sigma
//            stocks.add(new EachStock("Tesla"));//gem
//            stocks.add(new EachStock("Netflix"));//vidmate
//            stocks.add(new EachStock("Apple"));//mito
//            stocks.add(new EachStock("Microsoft"));//nebula
//            stocks.add(new EachStock("Facebook"));//buddymate
//
//            if(round == 3){
//                stocks.add(new EachStock("Android"));//orbilon
//            }
//            if(round < 3){
//                stocks.add(new EachStock("Nokia"));//doro
//            }



            stocks.add(new EachStock("Sigma"));//sigma
            stocks.add(new EachStock("Davenport"));//gem
            stocks.add(new EachStock("VidMate"));//vidmate
            stocks.add(new EachStock("Mito"));//mito
            stocks.add(new EachStock("Nebula"));//nebula
            stocks.add(new EachStock("Buddy Mate"));//buddymate

            if(round == 2){
                stocks.add(new EachStock("Orbilon"));//orbilon
            }
            if(round < 4){
                stocks.add(new EachStock("Doro"));//doro
            }
        }
        else if(day ==2){
            stocks.add(new EachStock("Jio"));
            stocks.add(new EachStock("Patanjali"));
            stocks.add(new EachStock("Amazon"));
            stocks.add(new EachStock("Tata"));
            stocks.add(new EachStock("Kingfisher"));
            stocks.add(new EachStock("Ola"));

        }
        else if(day==4){
            stocks.add(new EachStock("Humans"));
            stocks.add(new EachStock("Meta Humans"));
            stocks.add(new EachStock("Mutual Funds"));
            stocks.add(new EachStock("Artificial Intelligence"));
            stocks.add(new EachStock("Orcs"));
            int round = GetData.getRound();
            if(round>1){
                stocks.add(new EachStock("Cyborgs"));
            }




        }

        return stocks;
    }


    public static String desc(String name){
        if(name.equals("VidMate")){
            return "Online streaming service";
        }

        if(name.equals("Orbilon")){
            return "Smart phone brand";
        }

        if(name.equals("Sigma")){
            return "Internet services company";
        }

        if(name.equals("GEM")){
            return "Billionaire entrepreneur";
        }

        if(name.equals("Doro")){
            return "Feature phone company";
        }

        if(name.equals("Buddy Mate")){
            return "Social Media Site";
        }

        if(name.equals("Mito")){
            return "One of the tech Giant";
        }
        if(name.equals("Nebula")){
            return "One of the tech Giant";
        }
        if(name.equals("Iron Man")){
            return "Lokhnadi Manus";
        }
        if(name.equals("Captain America")){
            return "Mein hu is USA ka Jaykant Shikre";
        }
        if(name.equals("Thanos")){
            return "Chamkila Chamkila Chamkila re";
        }

        if(name.equals("Amazon")){
            return "The world's largest online shopping website";
        }





        if(name.equals("Patanjali")){
            return "Ayurvedic medicines company by Baba Ramdev";
        }





        if(name.equals("Jio")){
            return "4G network company with affordable prices";
        }




        if(name.equals("Ola")){
            return "A cab rental app which is a competitor to Uber";
        }





        if(name.equals("Kingfisher")){
            return "An airline venture by Vijay Mallya";
        }





        if(name.equals("Tata")){
            return "A multinational car manufacturing company by TATA group";
        }





        return null;
    }


}
