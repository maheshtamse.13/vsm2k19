package propya.mr.vsm.Story;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import propya.mr.vsm.Activities.MainActivity;

public class TimerService extends Service {

    public static int secs;
    CountDownTimer countDownTimer;
    int i=0,j=0,m=0,k=0;
    public static boolean isRunning = false;
    int timeRemain;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
   //     Toast.makeText(this, "Timer started", Toast.LENGTH_SHORT).show();
        timer(secs);
        return START_STICKY;

    }




    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void timer(final int secs){
//        m=secs+1;
//
//        j=m/60;
//        k=j;
//        i=m%60;
         timeRemain = secs;
        countDownTimer=new CountDownTimer(secs*1000,1000) {
            @Override
            public void onTick(long l) {
//                i--;
//                if(k!=j)
//                {
//                    i=59;
//                    k=j;
//                }
//                if(i==0){
//                    j--;
//                }
                timeRemain--;
                Log.i("TIMER",Integer.toString(timeRemain));
//                if(MainActivity.timerDisplay !=null){
//                    MainActivity.timerDisplay.setTitle(Integer.toString(timeRemain));
//                }
            }

            @Override
            public void onFinish() {
                isRunning = false;
      //          Toast.makeText(TimerService.this, "FINISH!!", Toast.LENGTH_SHORT).show();
            }
        };
        countDownTimer.start();
        isRunning = true;
    }
}
