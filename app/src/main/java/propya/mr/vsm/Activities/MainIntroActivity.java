package propya.mr.vsm.Activities;

import android.Manifest;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.app.OnNavigationBlockedListener;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import com.heinrichreimersoftware.materialintro.slide.Slide;

import propya.mr.vsm.Fragments.LoginFragment;
import propya.mr.vsm.R;

public class MainIntroActivity extends IntroActivity {

    public static final String EXTRA_FULLSCREEN = "com.heinrichreimersoftware.materialintro.demo.EXTRA_FULLSCREEN";
    public static final String EXTRA_SCROLLABLE = "com.heinrichreimersoftware.materialintro.demo.EXTRA_SCROLLABLE";
    public static final String EXTRA_CUSTOM_FRAGMENTS = "com.heinrichreimersoftware.materialintro.demo.EXTRA_CUSTOM_FRAGMENTS";
    public static final String EXTRA_PERMISSIONS = "com.heinrichreimersoftware.materialintro.demo.EXTRA_PERMISSIONS";
    public static final String EXTRA_SHOW_BACK = "com.heinrichreimersoftware.materialintro.demo.EXTRA_SHOW_BACK";
    public static final String EXTRA_SHOW_NEXT = "com.heinrichreimersoftware.materialintro.demo.EXTRA_SHOW_NEXT";
    public static final String EXTRA_SKIP_ENABLED = "com.heinrichreimersoftware.materialintro.demo.EXTRA_SKIP_ENABLED";
    public static final String EXTRA_FINISH_ENABLED = "com.heinrichreimersoftware.materialintro.demo.EXTRA_FINISH_ENABLED";
    public static final String EXTRA_GET_STARTED_ENABLED = "com.heinrichreimersoftware.materialintro.demo.EXTRA_GET_STARTED_ENABLED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        setFullscreen(false);
//        super.onCreate(savedInstanceState);

//        Intent intent = getIntent();


        super.onCreate(savedInstanceState);
        boolean fullscreen = false;  //intent.getBooleanExtra(EXTRA_FULLSCREEN, false);
        boolean scrollable = false; //intent.getBooleanExtra(EXTRA_SCROLLABLE, false);
        boolean customFragments = true; //intent.getBooleanExtra(EXTRA_CUSTOM_FRAGMENTS, true);
        boolean permissions = true; //intent.getBooleanExtra(EXTRA_PERMISSIONS, true);
        boolean showBack = true; //intent.getBooleanExtra(EXTRA_SHOW_BACK, true);
        boolean showNext = true; //intent.getBooleanExtra(EXTRA_SHOW_NEXT, true);
        boolean skipEnabled = true; //intent.getBooleanExtra(EXTRA_SKIP_ENABLED, true);
        boolean finishEnabled = true; //intent.getBooleanExtra(EXTRA_FINISH_ENABLED, true);
        boolean getStartedEnabled = true; //intent.getBooleanExtra(EXTRA_GET_STARTED_ENABLED, true);


        setButtonBackFunction(skipEnabled ? BUTTON_BACK_FUNCTION_SKIP : BUTTON_BACK_FUNCTION_BACK);
        setButtonNextFunction(
                finishEnabled ? BUTTON_NEXT_FUNCTION_NEXT_FINISH : BUTTON_NEXT_FUNCTION_NEXT);
        setButtonBackVisible(showBack);
        setButtonNextVisible(showNext);
        setButtonCtaVisible(getStartedEnabled);
        setButtonCtaTintMode(BUTTON_CTA_TINT_MODE_TEXT);

        addSlide(new SimpleSlide.Builder().title("Welcome to VSM").description("Get ready to play the most exciting market simulation game ever!!")
                .image(R.drawable.ic_vsmlogo)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .build()
        );


        addSlide(new SimpleSlide.Builder()
                .title("Stalk the Stock")
                .description("Keep your eyes wide open and observe the stocks carefully. You may never know what might happen!")
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .scrollable(scrollable)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Be Prepared!")
                .description("Keep a pen and paper with you, you are going to need it!")
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .scrollable(scrollable)
                .buttonCtaLabel("Hello")
                .buttonCtaClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast toast = Toast
//                                .makeText(MainIntroActivity.this, R.string.toast_button_cta, Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

                        nextSlide();
                    }
                })
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Follow the Rules")
                .description("Follow the rules that will be shown in the presentation, otherwise you may get disqualified.")
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .scrollable(scrollable)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("\n\nSpecial Thanks")
                .image(R.drawable.ic_sponsorvsm)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .scrollable(scrollable)
                .build());

        final Slide permissionsSlide;
        if (permissions) {
            permissionsSlide = new SimpleSlide.Builder()
                    .title(R.string.title_permissions)
                    .description(R.string.description_permissions)
                    .background(R.color.colorPrimary)
                    .backgroundDark(R.color.colorPrimaryDark)
                    .scrollable(scrollable)
                    .permissions(new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.ACCESS_COARSE_LOCATION})
                    .buttonCtaLabel("Grant Permisisions")
                    .buttonCtaClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            nextSlide();
                        }
                    })
                    .build();
            addSlide(permissionsSlide);
        } else {
            permissionsSlide = null;
        }


        final Slide loginSlide;
        if (customFragments) {
            loginSlide = new FragmentSlide.Builder()
                    .background(R.color.colorPrimary)
                    .backgroundDark(R.color.colorPrimaryDark)
                    .fragment(LoginFragment.newInstance())
                    .build();
            addSlide(loginSlide);

//            addSlide(new FragmentSlide.Builder()
//                    .background(R.color.color_custom_fragment_2)
//                    .backgroundDark(R.color.color_dark_custom_fragment_2)
//                    .fragment(R.layout.fragment_custom, R.style.AppThemeDark)
//                    .build());
        } else {
            loginSlide = null;
        }

        addSlide(new SimpleSlide.Builder()
                .title("Hurrayyy!!")
                .description("Welcome to the VSM app! The game will start shortly, please wait patiently....")
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .scrollable(scrollable)
                .build());




//        setNavigationPolicy(new NavigationPolicy() {
//            @Override
//            public boolean canGoForward(int position) {
//                if(position==4||position==5)
//                    return false;
//                else
//                    return true;
//            }
//
//            @Override
//            public boolean canGoBackward(int position) {
//                return true;
//            }
//        });

        addOnNavigationBlockedListener(new OnNavigationBlockedListener() {
            @Override
            public void onNavigationBlocked(int position, int direction) {
                View contentView = findViewById(android.R.id.content);
                if (contentView != null) {
                    Slide slide = getSlide(position);

                    if (slide == permissionsSlide) {
                        Snackbar.make(contentView, R.string.label_grant_permissions, Snackbar.LENGTH_LONG)
                                .show();
                    } else if (slide == loginSlide) {
                        Snackbar.make(contentView, R.string.label_fill_out_form, Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

        //Feel free to add and remove page change listeners
        /*
        addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        */
    }

}
