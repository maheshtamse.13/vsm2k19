package propya.mr.vsm.Activities;

import android.animation.Animator;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.utils.UrlBeaconUrlCompressor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import propya.mr.vsm.Fragments.AboutUs;
import propya.mr.vsm.Fragments.AnimationMiddle;
import propya.mr.vsm.Fragments.GetString;
import propya.mr.vsm.Fragments.LeftMost;
import propya.mr.vsm.Fragments.LyricsDisplay;
import propya.mr.vsm.Fragments.MainFrag;
import propya.mr.vsm.Fragments.Stats;
import propya.mr.vsm.Fragments.ThankYou;
import propya.mr.vsm.Fragments.WaitForBeacon;
import propya.mr.vsm.Listeners.BluetoothHelpers;
import propya.mr.vsm.Listeners.StartTimerNextRound;
import propya.mr.vsm.Listeners.ViewLoaded;
import propya.mr.vsm.R;
import propya.mr.vsm.Story.Constants;
import propya.mr.vsm.Story.GetData;
import propya.mr.vsm.UI.AnimateMiddle;
import propya.mr.vsm.UI.AppStatus;
import propya.mr.vsm.UI.CurvedBottomNavigationView;

import static propya.mr.vsm.UI.AppStatus.getContext;

public class MainActivity extends AppCompatActivity  {

    static CurvedBottomNavigationView bottomNav;
    MainFrag mainFrag;
    Stats stats;
    AboutUs aboutUs;
    static FloatingActionButton middleButton;
    MenuItem bottomItem;
    LayoutInflater inflater;
    ViewGroup container;
    public static MenuItem timerDisplay;
    View rootView;
    public static StartTimerNextRound timer;
    public static BluetoothHelpers bluetoothHelpers;
    static BeaconManager manager;
    static WaitForBeacon beacon;

    public static AnimationMiddle getBeacon() {
        if(beacon == null){
            beacon = new WaitForBeacon();
        }
        return beacon;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO: Uncomment startActivity
//        startActivity(new Intent(MainActivity.this, MainIntroActivity.class));
        super.onCreate(savedInstanceState);


        setContentView(R.layout.useless);
        bottomNav = (CurvedBottomNavigationView) findViewById(R.id.bottomNav);

        rootView = findViewById(R.id.rootViewMain);

        middleButton = (FloatingActionButton) findViewById(R.id.middleFrag);
        middleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bottomItem != null) {
                    bottomItem.setChecked(false);
                    bottomItem.setChecked(false);
                }

                bottomNav.setSelectedItemId(R.id.middleMenu);
                if (mainFrag == null) {
                    mainFrag = new MainFrag();
                }


                swapMiddleFrag();
//                 getSupportFragmentManager().beginTransaction().replace(R.id.fragment,mainFrag).commit();
//                revealAnim(mainFrag);
            }
        });


        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                bottomItem = item;

                if(AppStatus.roundTimerRunnig){
                    Snackbar.make(rootView,"Atleast focus while round is running",Snackbar.LENGTH_SHORT).show();
////TODO uncomment
                    return true;
                }


                switch (item.getItemId()) {
                    case R.id.leftMostMenu:
                        if (stats == null) {
                            stats = new Stats();
                        }
//                        return true;
                        swapFrags(stats);
                        return true;
                    case R.id.middle:
                        //Toast.makeText(MainActivity.this, "middle frag", Toast.LENGTH_SHORT).show();
//                        if (mainFrag == null) {
//                            mainFrag = new MainFrag();
//                        }
//                        swapFrags(mainFrag);
                        return true;
                    case R.id.rightMostMenu:
                        if (aboutUs == null) {
                            aboutUs = new AboutUs();
                        }
                    swapFrags(new LeftMost());
//                        swapFrags(aboutUs);

                        return true;


                }


                return false;
            }
        });



        middleButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));

        AppStatus.currentContextSecondary = this;


        timer = new StartTimerNextRound() {
            @Override
            public void roundStartsIn(long sec, final String fragToSwap) {
                if(AppStatus.beaconTimerRunnig){
                    return;
                }
                AppStatus.beaconTimerRunnig = true;
                CountDownTimer timer = new CountDownTimer(sec,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
//                        MainActivity.timerDisplay.setTitle("Sec Left:"+Long.toString(millisUntilFinished/1000));
                        long time=millisUntilFinished/1000;
                        try{


                        if(time<10){
                            if(time%2!=0)
                                middleButton.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                            else
                                middleButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                        }
                        middleButton.setImageBitmap(textAsBitmap(Long.toString(time), 40, Color.WHITE));
                    }catch (Exception e){

                        }
                    }

                    @Override
                    public void onFinish() {

                        middleButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                //        Toast.makeText(MainActivity.this, "Round "+GetData.getRound(), Toast.LENGTH_SHORT).show();
                        if(!GetData.incrementRound()){

                            try {
                                MainActivity.revealAnim(new ThankYou());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            return;
                        }
                        handleMerger();
                        AppStatus.beaconTimerRunnig = false;
                        AppStatus.beaconShouldScan = false;
//                        if(fragToSwap.equals("next")){
                            MainFrag frag = new MainFrag();
                        try {
                            revealAnim(frag);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        }
                        GetData.setAppStat(1);
                        GetData.setCashMenu();

                    }
                };
                timer.start();


            }
        };

//        setBluetoothHelpers();

//        swapMiddleFrag();
        stats = new Stats();
        swapFrags(stats);

    }

    private void swapMiddleFrag() {

//        bottomNav.findViewById(R.id.middleMenu).performClick();
//        Toast.makeText(MainActivity.this, "Swap", Toast.LENGTH_SHORT).show();
        if(AppStatus.fragment ==1){
            return;
        }
        int round = GetData.getRound();


        int appStat  = getSharedPreferences(Constants.sharedAppStat,MODE_PRIVATE).getInt(Constants.sharedAppStat,0);
        try{


        switch (appStat) {
            case 0:
                // Beacon

                revealAnim(new WaitForBeacon());
                break;
            case 1:
                //round frag
                if(GetData.getDay()==0){

                }
                revealAnim(new MainFrag());

                break;
            case 2:
                // lyrics
                if(GetData.getDay()==0){
                    revealAnim(getBeacon());
                    break;
                }
                revealAnim(new LyricsDisplay());

                break;

        }


        }catch (Exception e){

        }





















    }


    void swapFrags(Fragment frags){
        if (frags == null) {
            return;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, frags).commit();


    }



    //method to convert your text to image
    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }


    @Override
    protected void onResume() {
        super.onResume();
//        if(AppStatus.beaconShouldScan){
//            if(bluetoothHelpers!=null){
//                bluetoothHelpers.listenForBeacons();
//            }
//        }
//
//        swapMiddleFrag();
        if(!GetData.isLoggedIn()){
            Intent i = new Intent(this,MainIntroActivity.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(bluetoothHelpers!=null){
            bluetoothHelpers.stopScan();
        }
    }


    //Circular Reveal Animation - https://developer.android.com/training/animation/reveal-or-hide-view


    public static void revealAnim(AnimationMiddle fragment) throws Exception {
//        final MainFrag mainFrag = (MainFrag) fragment;
        bottomNav.getMenu().getItem(0).setChecked(false);

        bottomNav.getMenu().getItem(2).setChecked(false);
        bottomNav.getMenu().getItem(1).setChecked(true);
        final AnimationMiddle mainFrag = fragment;
//        swapFrags(mainFrag);

        ViewLoaded listener = new ViewLoaded() {
            @Override
            public void viewHasLoaded(View myView) {

                if(myView == null){
                    return;
                }

                Log.i("ANIMATE", "callback received");

                myView.setVisibility(View.VISIBLE);


// Check if the runtime version is at least Lollipop
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // get the center for the clipping circle
                    int cx = myView.getWidth() / 2;
                    int cy = myView.getHeight() / 2;
                    int dim[] = new int[2];
                    middleButton.getLocationOnScreen(dim);
                    AnimateMiddle.cx = dim[0];
                    AnimateMiddle.cy = dim[1];


                    AnimateMiddle.c = AppStatus.getContext();
//                    try{


                    Animator anim = AnimateMiddle.getAnimatoin(myView);

                    Log.i("ANIMATE", Integer.toString(dim[1]));
                    // make the view visible and start the animation
                    myView.setVisibility(View.VISIBLE);
                    anim.start();
                    anim.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            mainFrag.hidePink();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
//                }catch (Exception e){
//
//                    }

                } else {
                    // set the view to visible without a circular reveal animation below Lollipop
                    mainFrag.hidePink();
                    myView.setVisibility(View.VISIBLE);
                }
            }
        };

        mainFrag.setListener(listener);
        ((AppCompatActivity)AppStatus.getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();

//        View myView = (View) inflater.inflate(fragment_main, container,  false);
//       View myView =  LayoutInflater.from(this).inflate(fragment_main,(ViewGroup) getWindow().getDecorView().getRootView(),false);
     /*  myView = mainFrag.getView();
       myView.setVisibility(View.INVISIBLE);


// Check if the runtime version is at least Lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = myView.getWidth() / 2;
            int cy = myView.getHeight() / 2;

            // get the final radius for the clipping circle
            float finalRadius = (float) Math.hypot(cx, cy);

            // create the animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0f, finalRadius);

            // make the view visible and start the animation
            myView.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            // set the view to visible without a circular reveal animation below Lollipop
            myView.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        setTimerDisplay(menu);
        GetData.setCashMenu();
        return true;
    }







    private void setTimerDisplay(Menu menu) {
        timerDisplay = menu.findItem(R.id.timerAction);

    }

    public static FloatingActionButton getMiddleButton() {
        return middleButton;
    }


    public Fragment middleFragment(){





        return null;
    }


    static Region region;
    static void setBluetoothHelpers(){


        bluetoothHelpers = new BluetoothHelpers() {
            @Override
            public void enableScan() {
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                if(!adapter.isEnabled()){
              //      Toast.makeText(getContext(), "Trying to enable ", Toast.LENGTH_SHORT).show();
                    adapter.enable();

                    Handler h = new Handler();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(AppStatus.beaconTimerRunnig){
                                listenForBeacons();
                            }

                        }
                    }, 5000);


                }else{
                    listenForBeacons();
                }

            }

            @Override
            public void stopScan() {
                if(manager != null){
                    if(manager.isBound(this)){
                        manager.unbind(this);
                    }
                    try {
                        manager.stopRangingBeaconsInRegion(region);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                BluetoothAdapter.getDefaultAdapter().disable();


            }

            @Override
            public void disableBluetooth() {
                BluetoothAdapter.getDefaultAdapter().disable();
            }

            @Override
            public void listenForBeacons() {

                manager = BeaconManager.getInstanceForApplication(getContext());
                manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_URL_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_TLM_LAYOUT));
//        manager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT));
                manager.setBackgroundMode(false);
                manager.bind(this);


            }

            @Override
            public void setTimer(int hh, int mm) {
                Log.i("BEACIN","SETTING timer");
                Date date = Calendar.getInstance().getTime();
//        DateFormat format = DateForm
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy ,hh:mm:ss");
                Time time = new Time();
                time.setToNow();
                dateFormat.setTimeZone(TimeZone.getDefault());


                String timeCurrent = dateFormat.format(date);

                String reminder = timeCurrent.split(",")[0];
                reminder+=String.format(",%d:%d:00",hh,mm);



                long diff =0;
                try {

                    Log.i("BEACIN",timeCurrent+ dateFormat.parse(timeCurrent).getTime());
                    Log.i("BEACIN",reminder+dateFormat.parse(reminder).getTime());



                    diff = dateFormat.parse(reminder).getTime();
                    diff-= dateFormat.parse(timeCurrent).getTime();
                }catch (Exception e){
                    Log.i("BEACIN",Long.toString(diff));
                }

                Log.i("BEACIN","diff 0"+Long.toString(diff));
                if(diff == 0){

                    return;
                }

                stopScan();
                timer.roundStartsIn(diff,"next");
            }

            @Override
            public boolean isListening() {
                return false;
            }

            @Override
            public void onBeaconServiceConnect() {
                Log.i("BEACIN","Connected");

                ArrayList<Identifier> identifiers = new ArrayList<>();
                identifiers.add(null);

                region = new Region("All",identifiers);

                try {
                    manager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }


                RangeNotifier rangeNotifier = new RangeNotifier() {
                    @Override
                    public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                        Log.i("BEACIN","did range");
                        Log.i("BEACIN",Integer.toString(collection.size()));
                        for(Beacon c : collection){
                            Log.i("BEACIN",c.toString());
                            Log.i("BEACIN code",Integer.toString(c.getBeaconTypeCode()));
                            if (c.getServiceUuid() == 0xfeaa && c.getBeaconTypeCode() == 0x10) {
                                Log.i("BEACIN","edy url agaya");
                                // This is a Eddystone-URL frame
                                String url = UrlBeaconUrlCompressor.uncompress(c.getId1().toByteArray());
                                Log.d("BEACIN", "I see a beacon transmitting a url: " + url +
                                        " approximately " + c.getDistance() + " meters away.");
                                if(isVsmUrl(url)){
                                    startTimer(url);
                                }
                            }
                        }
                    }
                };
                manager.addRangeNotifier(rangeNotifier);
            }

            @Override
            public Context getApplicationContext() {
                return getContext();
            }

            @Override
            public void unbindService(ServiceConnection serviceConnection) {
                getContext().unbindService(serviceConnection);
            }

            @Override
            public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
                return getContext().bindService(intent,serviceConnection,i);
            }
        };










































    }

    static boolean isVsmUrl(String url){

        try {
            String s = url.split("//")[1];
            s = s.split("\\.")[0];
            return s.equals("vsm");
        }catch (Exception e){
            return false;
        }




    }

    static private void startTimer(String url) {

        try {


            String u[] = url.split("\\.");
            int hr = Integer.parseInt(u[1]);
            int min = Integer.parseInt(u[2]);

       //     Toast.makeText(getContext(), "The round starts at " + u[1] + ":" + u[2], Toast.LENGTH_SHORT).show();
            bluetoothHelpers.stopScan();
            bluetoothHelpers.setTimer(hr,min);

        }catch (Exception e){

        }

    }


    void handleMerger(){
        //after stocks have been increment
        int round = GetData.getRound();
        int day = GetData.getDay();
        if(day ==1){

            Snackbar.make(rootView,"Round: "+round,Snackbar.LENGTH_SHORT).show();
            if(round==3){
                //handle merger for android
                GetData.handleMerger("sigma","orbilon",2);

            }

            if(round ==4){
                //handle merger for nokia
                GetData.handleMerger("nebula","doro",3);



            }




        }




    }




}