package propya.mr.vsm.Activities;

import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import propya.mr.vsm.R;

public class Intro extends IntroActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlide(new SimpleSlide.Builder().title("Welcome to VSM").description("Ruchi please write a good desc")
                .image(R.drawable.ic_vsmlogo)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorPrimaryDark)
                .build()
        );


    }
}
